from django.shortcuts import render
from django.http import HttpResponse
from asgiref.sync import async_to_sync
from django.http import HttpResponse
from channels.layers import get_channel_layer

# Create your views here.

def chat(requset):
    room_name = requset.GET.get("room_name",0)
    return render(requset, 'chat/lobby.html', context={"room_name":room_name})


channel_layer = get_channel_layer()

def send(requset):
    room_name = requset.POST["room_name"]
    message = requset.POST["message"]
    print(message)
    print(room_name)

    async_to_sync(channel_layer.group_send)(room_name, {
        'type': 'chat_message',
        'message': message
    })
    return HttpResponse("ok")
