from django.urls import path
from . import views

urlpatterns=[
    path('chats', views.chat),
    path('send-message', views.send)
]